package com.rostyslavprotsiv.view.interfaces;

@FunctionalInterface
public interface GetableExample {
    void getExample(StringBuilder b);
}
