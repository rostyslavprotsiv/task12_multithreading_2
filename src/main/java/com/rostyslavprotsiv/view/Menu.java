package com.rostyslavprotsiv.view;

import com.rostyslavprotsiv.view.interfaces.GetableExample;

public final class Menu extends AbstractMenu {

    public Menu() {
        menu.put(0, "Print example with the same object in synchronization " +
                "blocks");
        menu.put(1, "Print example with many different objects in " +
                "synchronization blocks");
        menu.put(2, "Show communication between two tasks with the help of " +
                "pipes");
        menu.put(3, "Show example with my own ReadWriteLock : ");
        menu.put(4, "Exit");
        methodsForMenu.put(0, this::printExampleWithOneObj);
        methodsForMenu.put(1, this::printExampleWithDifObj);
        methodsForMenu.put(2, this::showWriteAndRead);
        methodsForMenu.put(3, this::printExample);
        methodsForMenu.put(4, this::quit);
    }

    public void show() {
        out();
    }

    @Override
    protected void showInfo() {
        LOGGER.info("Menu for task12_MultiThreading_2");
        menu.forEach((key, elem) -> LOGGER.info(key + " : " + elem));
    }

    private void printExampleWithOneObj() {
        printExampleWith(CONTROLLER::getExampleWithOneObj);
    }

    private void printExampleWith(GetableExample get) {
        StringBuilder b = new StringBuilder();
        Thread listener = new Thread(() -> bListener(b));
        listener.start();
        get.getExample(b);
        try {
            listener.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void printExampleWithDifObj() {
        printExampleWith(CONTROLLER::getExampleWithDifObj);
    }

    private void bListener(StringBuilder b) {
        int maxCalls = 3;
        StringBuilder bOld = new StringBuilder(b);
        int counter = 0;
        while (counter != maxCalls) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                String logMessage = e.getMessage();
                LOGGER.info(logMessage);
                LOGGER.warn(logMessage);
                LOGGER.error(logMessage);
                LOGGER.fatal(logMessage);
                e.printStackTrace();
            }
            if (!b.toString().equals(bOld.toString())) {
                LOGGER.info(b.toString());
                bOld = new StringBuilder(b);
                counter++;
            }
        }
    }

    private void showWriteAndRead() {
        StringBuilder ch = new StringBuilder();
        CONTROLLER.listenQueue(ch);
        listenInput(ch);
    }

    private void listenInput(StringBuilder ch) {
        char letter;
        while (true) {
            LOGGER.info("Please, input one char");
            if((letter = scan.next(".").charAt(0)) == 'Q') {
                quit();
                break;
            }
            CONTROLLER.putLetter(letter);
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            LOGGER.info(ch.toString());
        }
    }

    private void printExample() {
        StringBuilder b = new StringBuilder();
        new Thread(()->exampleListener(b)).start();
        CONTROLLER.getExample(b);
    }

    private void exampleListener(StringBuilder b) {
        int counter = 6;
        int previousSize = 0;
        while(counter != 0) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(b.length() > previousSize) {
                LOGGER.info(b.toString());
                previousSize = b.length();
                counter --;
            }
        }
    }
}
