package com.rostyslavprotsiv.model.action;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SynchronizationExampleAction {
    private final static Lock LOCK = new ReentrantLock();
    private final static Lock LOCK1 = new ReentrantLock();
    private final static Lock LOCK2 = new ReentrantLock();
    private final static Lock LOCK3 = new ReentrantLock();

    private void doSmth(StringBuilder b) {
        LOCK.lock();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (b.length() == 0) {
                b.append(2);
            } else {
                b.deleteCharAt(0);
                b.append(2);
            }
            LOCK.unlock();
    }

    private void doSmth1(StringBuilder b) {
        //some logic
        LOCK.lock();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (b.length() == 0) {
                b.append(3);
            } else {
                b.deleteCharAt(0);
                b.append(3);
            }
        LOCK.unlock();
    }

    private void doSmth2(StringBuilder b) {
        //some logic
        LOCK.lock();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (b.length() == 0) {
                b.append(4);
            } else {
                b.deleteCharAt(0);
                b.append(4);
            }
        LOCK.unlock();
    }

    private void doSmthWithOther(StringBuilder b) {
        //some logic
        LOCK1.lock();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (b.length() == 0) {
                b.append(2);
            } else {
                b.deleteCharAt(0);
                b.append(2);
            }
        LOCK1.unlock();
    }

    private void doSmthWithOther1(StringBuilder b) {
        //some logic
        LOCK2.lock();
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (b.length() == 0) {
                b.append(3);
            } else {
                b.deleteCharAt(0);
                b.append(3);
            }
        LOCK2.unlock();
    }

    private void doSmthWithOther2(StringBuilder b) {
        //some logic
        LOCK3.lock();
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (b.length() == 0) {
                b.append(4);
            } else {
                b.deleteCharAt(0);
                b.append(4);
            }
        LOCK3.unlock();
    }

    public void showExampleWithOneObj(StringBuilder b) {
        new Thread(() -> doSmth(b)).start();
        new Thread(() -> doSmth1(b)).start();
        new Thread(() -> doSmth2(b)).start();
    }

    public void showExampleWithDifObj(StringBuilder b) {
        new Thread(() -> doSmthWithOther(b)).start();
        new Thread(() -> doSmthWithOther1(b)).start();
        new Thread(() -> doSmthWithOther2(b)).start();
    }
}
