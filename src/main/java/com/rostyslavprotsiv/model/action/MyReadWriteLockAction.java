package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.MyReadWriteLock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;

public class MyReadWriteLockAction {
    private final Logger LOGGER = LogManager.getLogger(MyReadWriteLockAction.class);
    private static ReadWriteLock l = new MyReadWriteLock();
    private static Lock readLock1 = l.readLock();
    private static Lock writeLock1 = l.writeLock();
    private static long A = 0;

    private Runnable runnable(StringBuilder b) {
        return () -> {
            readLock1.lock();
            try {
                b.append(LocalDateTime.now())
                        .append(Thread.currentThread()
                                .getName())
                        .append(" A=")
                        .append(A);
                readLock1.unlock();
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                String logMessage = e.getMessage();
                LOGGER.info(logMessage);
                LOGGER.error(logMessage);
                LOGGER.fatal(logMessage);
                e.printStackTrace();
            }
        };
    }

    public void example(StringBuilder b) {
        ExecutorService executor = Executors.newFixedThreadPool(4);
        Thread rT1 = new Thread(runnable(b));
        Thread rT2 = new Thread(runnable(b));
        Thread rT3 = new Thread(runnable(b));
        executor.submit(rT1);
        executor.submit(rT2);
        executor.submit(rT3);
        executor.submit(() -> {
            writeLock1.lock();
            A = 25;
            b.append(LocalDateTime.now())
                    .append(Thread.currentThread()
                            .getName())
                    .append(" A=")
                    .append(A);
            writeLock1.unlock();
        });
        executor.submit(rT1);
        executor.submit(rT2);
        executor.submit(rT3);
    }
}
