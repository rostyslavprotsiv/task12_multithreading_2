package com.rostyslavprotsiv.model.action;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class PipeCommunicationAction {
    private BlockingQueue<Character> text;

    public PipeCommunicationAction() {
        text = new LinkedBlockingQueue<>();
    }

    public void putLetter(Character letter) {
        new Thread(runnable(letter)).start();
    }

    public void listenQueue(StringBuilder ch) {
        new Thread(runnable1(ch)).start();
    }

    private Runnable runnable(Character letter) {
        return () -> {
            try {
                text.put(letter);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
    }

    private Runnable runnable1(StringBuilder ch) {
        return () -> {
            while (true) {
                try {
                    ch.replace(0, 1, String.valueOf(text.take()));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
    }
}
