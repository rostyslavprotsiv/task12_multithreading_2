package com.rostyslavprotsiv.model.entity;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;

public class MyReadWriteLock implements ReadWriteLock {
    private List<SpinLock> locksForRead = new LinkedList<>();
    private SpinLock lockForWrite;
    private ReadLock readLock = new ReadLock();
    private WriteLock writeLock = new WriteLock();

    @Override
    public Lock readLock() {
        return readLock;
    }

    @Override
    public Lock writeLock() {
        return writeLock;
    }

    private class ReadLock implements Lock {

        @Override
        public void lock() {
            if (lockForWrite == null) {
                SpinLock spinLock = new SpinLock();
                locksForRead.add(spinLock);
                spinLock.lock();
            } else {
                throw new IllegalThreadStateException();
            }
        }

        @Override
        public void lockInterruptibly() throws InterruptedException {
        }

        @Override
        public boolean tryLock() {
            return false;
        }

        @Override
        public boolean tryLock(long l, TimeUnit timeUnit) throws InterruptedException {
            return false;
        }

        @Override
        public void unlock() {
            if (lockForWrite == null) {
                SpinLock spinLock =
                        locksForRead.remove(locksForRead.size() - 1);
                spinLock.unlock();
            } else {
                throw new IllegalThreadStateException();
            }
        }

        @Override
        public Condition newCondition() {
            return null;
        }
    }

    private class WriteLock implements Lock {

        @Override
        public void lock() {
            if (locksForRead.isEmpty() && lockForWrite == null) {
                lockForWrite = new SpinLock();
                lockForWrite.lock();
            } else {
                throw new IllegalThreadStateException();
            }
        }

        @Override
        public void lockInterruptibly() throws InterruptedException {
        }

        @Override
        public boolean tryLock() {
            return false;
        }

        @Override
        public boolean tryLock(long l, TimeUnit timeUnit) throws InterruptedException {
            return false;
        }

        @Override
        public void unlock() {
            if (locksForRead.isEmpty() && lockForWrite != null) {
                lockForWrite.unlock();
                lockForWrite = null;
            } else {
                throw new IllegalThreadStateException();
            }
        }

        @Override
        public Condition newCondition() {
            return null;
        }
    }

    private class SpinLock {
        private AtomicBoolean state = new AtomicBoolean(false);

        public void lock() {
            while (true) {
                while (state.get()) {
                }
                if (!state.getAndSet(true)) {
                    return;
                }
            }
        }

        public void unlock() {
            state.set(false);
        }
    }
}
