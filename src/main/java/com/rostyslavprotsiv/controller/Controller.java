package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class Controller {
    private final Logger LOGGER = LogManager.getLogger(Controller.class);
    private final SynchronizationExampleAction SYNCHRONIZATION_EXAMPLE_ACTION =
            new SynchronizationExampleAction();
    private final PipeCommunicationAction PIPE_COMMUNICATION_ACTION =
            new PipeCommunicationAction();
    private final MyReadWriteLockAction MY_READ_WRITE_LOCK_ACTION =
            new MyReadWriteLockAction();

    public void getExampleWithOneObj(StringBuilder b) {
        if(b != null) {
            String logMessage = "Error with b(!== null)";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
        }
        SYNCHRONIZATION_EXAMPLE_ACTION.showExampleWithOneObj(b);
    }

    public void getExampleWithDifObj(StringBuilder b) {
        SYNCHRONIZATION_EXAMPLE_ACTION.showExampleWithDifObj(b);
    }

    public void putLetter(Character letter) {
        PIPE_COMMUNICATION_ACTION.putLetter(letter);
    }

    public void listenQueue(StringBuilder b) {
        PIPE_COMMUNICATION_ACTION.listenQueue(b);
    }

    public void getExample(StringBuilder b) {
        MY_READ_WRITE_LOCK_ACTION.example(b);
    }

    /*
     */
}
